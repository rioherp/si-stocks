@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Inventory</h1>
    <div>
    <a href="{{ url('stocks/create')}}" class="btn btn-primary mb-3">Add Stock</a>
    </div>
    @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Amount</td>
          <td>Price</td>
          <td>Updated at</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($stocks as $stock)
        <tr>
            <td>{{$stock->id}}</td>
            <td>{{$stock->name}} </td>
            <td>{{$stock->amount}}</td>
            <td>{{$stock->price}}</td>
            <td>{{$stock->updated_at}}</td>
            <td>
                <a href="{{ url('stocks/edit',$stock->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form method="post" action="{{ url('stocks/destroy', $stock->id)}}">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection
