<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StockController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/stocks', [StockController::class, 'index'])->name('stocks.index');

Route::get('/stocks/create', [StockController::class, 'create'])->name('stocks.create');

Route::post('/stocks/store', [StockController::class, 'store'])->name('stocks.store');

Route::get('/stocks/edit/{id}', [StockController::class, 'edit'])->name('stocks.edit');

Route::get('/stocks/update/{id}', [StockController::class, 'update'])->name('stocks.update');

Route::delete('/stocks/destroy/{id}', [StockController::class, 'destroy'])->name('stocks.destroy');
